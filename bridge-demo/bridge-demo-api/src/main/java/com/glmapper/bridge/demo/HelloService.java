package com.glmapper.bridge.demo;

/**
 * description
 *
 * @author: Jerry
 * @date: 2018/6/18
 */
public interface HelloService {

    String sayHello();
}
